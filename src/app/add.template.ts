import {AfterViewInit, Component, TemplateRef, ViewChild, ViewContainerRef, ViewRef} from '@angular/core';
import { ComponentFactoryResolver, NgZone, VERSION } from '@angular/core';
import { Input, OnInit, OnDestroy } from '@angular/core';


import { Directive } from '@angular/core';

import { AdComponent }      from './ad.component';

@Component({
  template: `
  <div>

  <mat-form-field>
  <mat-select>
    <mat-option>None</mat-option>
    <mat-option value="1">Option 1</mat-option>
    <mat-option value="2">Option 2</mat-option>
    <mat-option value="3">Option 3</mat-option>
    <mat-option value="4">Option 4</mat-option>
</mat-select>
</mat-form-field>


</div>
<div>
<mat-checkbox>Check me!</mat-checkbox>
</div>
  `
})
export class AddTemplate implements AdComponent {
  @Input() data: any;

}

/*


  <div><ng-container #vcm>lalalalala</ng-container></div>


    <div>
        <ng-container #vc></ng-container>
    </div>
      <ng-template #t1><span>I am SPAN from template 1</span></ng-template>
      <ng-template #t2><span>I am SPAN from template 2</span></ng-template>
  
  
    <div>
      <ng-container #ac></ng-container>
    </div>
    <ng-template #a1>
    <div><span>I am SPAN from template add</span>
    <div class="repeat">
    <mat-form-field id="value">
      <mat-select placeholder="จำนวนเงินที่ต้องการซื้อ * " [(value)]="selectedi">
        <mat-option>None</mat-option>
        <mat-option value="AE Dirham (AED)">AE Dirham (AED)</mat-option>
        <mat-option value="Australian Dollar (AUD)">Australian Dollar (AUD)</mat-option>
        <mat-option value="Bahraini Dinar (BHD)">Bahraini Dinar (BHD)</mat-option>
        <mat-option value="Brunei Dollar (BND)">Brunei Dollar (BND)</mat-option>
        <mat-option value="Swiss Franc (CHF)">Swiss Franc (CHF)</mat-option>
        <mat-option value="Chinee Yuan Renminbi (CNY)">Chinee Yuan Renminbi (CNY)</mat-option>
        <mat-option value="Danish Kroner (DKK)">Danish Kroner (DKK)</mat-option>
        <mat-option value="USD">USD</mat-option>
      </mat-select>
    </mat-form-field>                         
  </div>
  <!--div(click)="addAmount()" src="assets/mas.svg">lala</div-->
  
  <div class="left1">
  <mat-form-field class="letter">
    <input matInput [(ngModel)]="bitcoini" placeholder="จำนวนเงินที่ต้องการซื้อ * ">            
    <mat-icon style="font-size: 100px" matPrefix svgIcon="bit"></mat-icon>
  </mat-form-field>  
  <div id="addElements">
    <span class="etiqueta">จำนวนเงินบาท</span>
    <span class="etiqueta">อัตราแลกเปลี่ยน</span>
    <span class="etiqueta">ส่วนลด </span>
  </div>
  </div> 
  
  
          
    </div>
    </ng-template>    
    
  
  
    <ng-template #a2>
    <div><span>I am SPAN from template add2</span>
    <div class="repeat">
    <mat-form-field id="value">
      <mat-select placeholder="จำนวนเงินที่ต้องการซื้อ * " [(value)]="selectedj">
        <mat-option>None</mat-option>
        <mat-option value="AE Dirham (AED)">AE Dirham (AED)</mat-option>
        <mat-option value="Australian Dollar (AUD)">Australian Dollar (AUD)</mat-option>
        <mat-option value="USD">USD</mat-option>
      </mat-select>
    </mat-form-field>                         
  </div>
  
  <div class="left1">
  <mat-form-field class="letter">
    <input matInput [(ngModel)]="bitcoin" placeholder="จำนวนเงินที่ต้องการซื้อ * ">            
    <mat-icon style="font-size: 100px" matPrefix svgIcon="bit"></mat-icon>
  </mat-form-field>  
  <div id="addElements">
    <span class="etiqueta">จำนวนเงินบาท</span>
    <span class="etiqueta">อัตราแลกเปลี่ยน</span>
    <span class="etiqueta">ส่วนลด </span>
  </div>
  </div> 
  
  
          
    </div>
    </ng-template>
  


    <div class="job-ad">
      <h4>div</h4>

    </div>


*/