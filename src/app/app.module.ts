import { BrowserModule }        from '@angular/platform-browser';
import { NgModule }             from '@angular/core';
import { AppComponent }         from './app.component';
import { HeroJobAdComponent }   from './hero-job-ad.component';
import { AdBannerComponent }    from './ad-banner.component';
import { HeroProfileComponent } from './hero-profile.component';
import { AdDirective }          from './ad.directive';
import { AdService }            from './ad.service';
import { AddTemplate } from './add.template';
import { MaterialModule } from './material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  imports: [ BrowserModule ,  BrowserAnimationsModule,  MaterialModule, 
  ],
  providers: [AdService],
  declarations: [ AppComponent,
                  AdBannerComponent,
                  AddTemplate,
                  HeroJobAdComponent,
                  HeroProfileComponent,
                  AdDirective ],
  entryComponents: [ HeroJobAdComponent, HeroProfileComponent,AddTemplate ],
  bootstrap: [ AppComponent ]
})
export class AppModule {
  constructor() {}
}

