import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatNativeDateModule,MatIconModule,MatDatepickerModule,MatCheckboxModule,MatSelectModule, MatButtonModule, MatToolbarModule,MatRadioModule, matTabsAnimations,MatInputModule, MatProgressSpinnerModule, MatCardModule } from '@angular/material';
import { HttpClientModule } from "@angular/common/http";


@NgModule({
  imports: [MatNativeDateModule, HttpClientModule, MatIconModule,MatDatepickerModule,MatButtonModule, MatRadioModule, MatToolbarModule,MatInputModule, MatProgressSpinnerModule, MatCardModule,MatSelectModule,MatCheckboxModule],
  exports: [MatNativeDateModule,HttpClientModule, MatIconModule,MatDatepickerModule,MatButtonModule, MatRadioModule,MatToolbarModule,MatInputModule, MatProgressSpinnerModule, MatCardModule,MatSelectModule, MatCheckboxModule]
})
export class MaterialModule { }